package main

import (
	"fmt"
	"gitlab.com/vearutop/breaking"
	"gitlab.com/vearutop/extensible"
)

type localEmbed struct {
	Option int64
}

type local struct {
	breaking.MyPublic
	localEmbed
}

func (l local) DoAnything() { println("local") }

func main() {
	a := extensible.New("Hi")
	a.DoWhatever(a)

	b := breaking.MyPublic{Param: "Hi"}
	b.DoWhatever(b)

	c := local{}
	c.Option = 123 // refers to localEmbed
	c.Param = "Hi"
	c.DoWhatever(c.MyPublic)
	c.DoAnything()

	fmt.Printf("%#v\n", c)
}
